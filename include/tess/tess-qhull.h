/*
 *   (C) 2015 by UChicago Argonne, LLC
 *   See COPYING in top-level directory.
 */

#define qh_QHimport
#include "qhull_a.h"
#include "tet.h"
#include "delaunay.h"

void gen_delaunay_output(facetT *facetlist, struct dblock_t *dblock);
void reorder_neighbors(struct dblock_t *dblock);

